// @ts-check
const { defineConfig, devices } = require('@playwright/test');

/**
 * @see https://playwright.dev/docs/test-configuration
 */
module.exports = defineConfig({
  testDir: './src/tests',
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: 'html',
  use: {
    trace: 'on-first-retry',
    // All requests we send go to this API endpoint.
    baseURL: 'https://dev-tiktok.ecomdy.com/',
    
    //'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTY5NDg5NzEwNjFmYTcyZWQ5NWY5NDYiLCJsb2NhbFVzZXJJZCI6MjIwNTEsInJvbGVzIjpbIlVTRVIiXSwiYmlsbGluZ3MiOnsic3RyaXBlIjp7ImFjY291bnRUeXBlIjoiVVMifX0sImlhdCI6MTcwMTYwNTAxNywiZXhwIjoxNzAxNjkxNDE3fQ.vK_xSiHmTrS7vKpHfxpvMurkvOy0ajeV2cTeUY_VHHo`,
    
  },
  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'],headless: false },
    },
  ],
});

