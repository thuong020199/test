import { Locator, Page, expect } from "@playwright/test";

export class PricingPlanPage {
    page: Page
    pageTitle: Locator
    textTrialDay: Locator
    titlePopularPlan: Locator
    titleEnterprisePlan: Locator
    dropDownListRegularPlan: Locator
    feeRegular1PLan: Locator
    limitAdsAccountRegular1: Locator
    upgradeRegularButton: Locator
    maximumAdsAccountRegular1: Locator
    limitSpendingRegular1: Locator
    trialDayRegular1: Locator
    feeEnterprisePlan: Locator
    upgradeEnterpriseButton: Locator
    maximumAdsAccountEnterprise: Locator
    limitSpendingEnterprise: Locator

    
    listCard: Locator
    addCreditCardButton: Locator
    closeButton: Locator
    addCardForm: Locator
    emailInput: Locator
    cardNumberInput: Locator
    expiredInput: Locator
    cvcInput: Locator
    nameOnCardInput: Locator
    addCardButton: Locator
    emailFiledMessageError: Locator
    cardNumberFieldNumberMessageError: Locator
    expiredFieldMessageError: Locator
    cvcFieldMessageError: Locator
    nameOnCardFieldMessageError: Locator
    listCreditCard: Locator

    constructor(page: Page) {
        this.page = page
        this.pageTitle = this.page.getByText(' Pricing Plan ')
        this.textTrialDay = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[1]/p')
        this.upgradeRegularButton = this.page.getByTestId('btn-upgrade-regular')
        this.listCard = this.page.getByTestId('card-pricing-management')
        this.addCreditCardButton = this.page.getByText('Add credit card')
        this.closeButton = this.page.getByText('Close')
        this.addCardForm = this.page.getByTestId('credit-form-controller')
        this.emailInput = this.page.getByPlaceholder("Cardholder's email")
        this.cardNumberInput = this.page
        .frameLocator('iframe[title="Secure card number input frame"]')
        .getByPlaceholder('1234 1234 1234 1234')
        this.expiredInput = this.page
        .frameLocator('iframe[title="Secure expiration date input frame"]')
        .getByPlaceholder('MM / YY')
        this.cvcInput = this.page
        .frameLocator('iframe[title="Secure CVC input frame"]')
        .getByPlaceholder('CVC')
        this.nameOnCardInput = this.page.getByPlaceholder('Name on card')
        this.addCardButton = this.page
        .getByTestId('credit-form-controller')
        .getByText(' Add card ')
        this.emailFiledMessageError = this.page
        .locator('//*[@id="credit-form-controller"]/div/div[1]/div/div')
        this.cardNumberFieldNumberMessageError = this.page
        .locator('//*[@id="credit-form-controller"]/div/div[2]/div/div[2]')
        this.expiredFieldMessageError = this.page
        .locator('//*[@id="credit-form-controller"]/div/div[3]/div[1]/div[2]')
        this.cvcFieldMessageError = this.page
        .locator('//*[@id="credit-form-controller"]/div/div[3]/div[2]/div[2]')
        this.nameOnCardFieldMessageError = this.page
        .locator('//*[@id="credit-form-controller"]/div/div[4]/div/div')
        this.titlePopularPlan = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[1]/div[1]/p[1]')
        this.feeRegular1PLan = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[1]/div[1]/div[2]/h1')
        this.maximumAdsAccountRegular1 = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[1]/div[3]/div[1]/span')
        this.limitSpendingRegular1 = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[1]/div[3]/div[2]/span')
        this.trialDayRegular1 = this.page.getByText('1 days trial')
        this.listCreditCard = this.page.locator('xpath=//div[contains(@id,"card-pricing")]')
        this.titleEnterprisePlan = this.page
        .locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[2]/div[1]/p[1]')
        this.feeEnterprisePlan = this.page
        .locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[2]/div[1]/div[2]/h1')
        this.maximumAdsAccountEnterprise = this.page
        .locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div[1]/span')
        this.limitSpendingEnterprise = this.page
        .locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div[2]/span')
        this.upgradeEnterpriseButton = this.page.getByTestId('btn-upgrade-enterprise')
    }
    async verifyTitlePricingPageShowIsCorrect(title) {
        const pageTitle = await this.pageTitle.textContent()
        await expect(title).toBe(pageTitle)
    }
    async verifyLinkPricingPageIsCorrect(page) {
        const url = await page.url();
        await expect(url).toBe('https://dev-tiktok.ecomdy.com/pricing');
    }
    async verifyTrialDayShowOnText(trialDay) {
        const trialDayActually = await this.textTrialDay.textContent()
        await expect(trialDayActually).toContain(trialDay)
    }
    async clickOnUpgradeRegularButton() {
        await this.upgradeRegularButton.click()
    }
    async verifyListCreditCardShow() {
        await expect(this.listCard).toBeVisible()
    }
    async clickOnAddCreditCardButton() {
        await this.addCreditCardButton.click()
    }
    async verifyHiddenAddCreditCardButtonAfterOpenAddCardForm() {
        await expect(this.addCreditCardButton).toBeHidden()
    }
    async verifyVisibleCloseButtonAfterOpenAddCardForm() {
        await expect(this.closeButton).toBeVisible()
    }
    async verifyAddCreditCardFormShow() {
        await expect(this.addCardForm).toBeVisible()
        await expect(this.emailInput).toBeVisible()
        await expect(this.cardNumberInput).toBeVisible()
        await expect(this.expiredInput).toBeVisible()
        await expect(this.cvcInput).toBeVisible()
        await expect(this.nameOnCardInput).toBeVisible()
        await expect(this.addCardButton).toBeVisible()
    }
    async clickOnCloseAddCardFormButton() {
        await this.closeButton.click()
    }
    async verifyAddCardFormIsClosed() {
        await expect(this.addCardForm).toBeHidden()
    }
    async clickAddCardButton() {
        await this.addCardButton.click()
        //await this.page.waitForTimeout(30000)
    }
    async verifyEmailMessageErrorShow(messageError) {
        const emailFieldMessageError = await this.emailFiledMessageError.textContent()
        await expect(emailFieldMessageError).toContain(messageError)
    }
    async verifyCardNumberMessageErrorShow(messageError) {
        const cardNumberFieldMessageError = await this.cardNumberFieldNumberMessageError.textContent()
        await expect(cardNumberFieldMessageError).toContain(messageError)
    }
    async verifyExpiredMessageErrorShow(messageError) {
        const expiredFieldMessageError = await this.expiredFieldMessageError.textContent()
        await expect(expiredFieldMessageError).toContain(messageError)
    }
    async verifyCVCMessageErrorShow(messageError) {
        const cvcFieldMessageError = await this.cvcFieldMessageError.textContent()
        await expect(cvcFieldMessageError).toContain(messageError)
    }
    async verifyNameCardMessageErrorShow(messageError) {
        const nameCardFieldMessageError = await this.nameOnCardFieldMessageError.textContent()
        await expect(nameCardFieldMessageError).toContain(messageError)
    }
    async enterCardData(email, numberCard, expired, cvc, nameOnCard) {
        await this.emailInput.type(email)
        await this.cardNumberInput.type(numberCard)
        await this.expiredInput.type(expired)
        await this.cvcInput.type(cvc)
        await this.nameOnCardInput.type(nameOnCard)
    }
    async verifyInfoRegularPlanShow(name, feePlan, maximumAdsAccount, limitSpending, trialDay) {
        const namePlan = await this.titlePopularPlan.textContent()
        const feeRegular1PLan = await this.feeRegular1PLan.textContent()
        const maximumAdsAccountRegular1 = await this.maximumAdsAccountRegular1.textContent()
        const limitSpendingRegular1 = await this.limitSpendingRegular1.textContent()
        const trialDayRegular1 = await this.trialDayRegular1.textContent()
        await expect(namePlan).toContain(name)
        await expect(feeRegular1PLan).toContain(feePlan)
        await expect(maximumAdsAccountRegular1).toContain(maximumAdsAccount)
        await expect(limitSpendingRegular1).toContain(limitSpending)
        await expect(trialDayRegular1).toContain(trialDay)
    }
    async verifyVisibleUpgradeRegularPlanButton() {
        await expect(this.upgradeRegularButton).toBeVisible()
    }
    async verifyInfoEnterprisePlanShow(name, feePlan, maximumAdsAccount, limitAdsAccount) {
        const namePlan = await this.titleEnterprisePlan.textContent()
        const feeEnterprisePlan = await this.feeEnterprisePlan.textContent()
        const maximumAdsAccountEnterprise = await this.maximumAdsAccountEnterprise.textContent()
        const limitSpendingEnterprise = await this.limitSpendingEnterprise.textContent()
        await expect(namePlan).toContain(name)
        await expect(feeEnterprisePlan).toContain(feePlan)
        await expect(maximumAdsAccountEnterprise).toContain(maximumAdsAccount)
        await expect(limitSpendingEnterprise).toContain(limitAdsAccount)
    }
    async verifyVisibleUpgradeEnterprisePlanButton() {
        await expect(this.upgradeEnterpriseButton).toBeVisible()
    }
    async countTotalCard() {
        await this.page.waitForTimeout(5000)
        const countCard = await this.listCreditCard.count();
        return countCard
    }
    async getListCreditCard(request, token) {
        const response = await request.get('api/billings/stripe/payment-methods?lang=en', {
            headers: {
                Authorization: `Bearer ${token}`,
              }
        })
        const listCard = await response.json()
        return listCard
    }
    async getTotalCard(response) {
        const getTotalCard = response.result.length
        return getTotalCard
    }
    async verifyTotalCardShowIsCorrect(totalCardOnUI, totalCardOnApi) {
        await expect(totalCardOnUI).toBe(totalCardOnApi)
    }
    async verifyTotalCardShowAfterAddCard(totalCardBeforeAddCard, totalCardAfterAddCard) {
        await expect(totalCardBeforeAddCard + 1).toBe(totalCardAfterAddCard)
    }
}

    

