import { Locator, Page } from "@playwright/test";

export class LoginPage {
    page: Page;
    email: Locator;
    password: Locator;
    buttonLogin: Locator;

    constructor(page: Page) {
        this.page = page;
        this.email = page.getByPlaceholder('Enter your email')
        this.password = page.getByPlaceholder('Password')
        this.buttonLogin = page.getByTestId('btn-login')
    }
    async goto() {
        await this.page.goto('https://dev-tiktok.ecomdy.com/login');
    }
    async enterLogin() {
        await this.email.fill("thuong.it020199+628@gmail.com");
        await this.password.fill("12341234")
    }
    async clickOnButtonLogin() {
        await this.buttonLogin.click()
    }
    async getToken(page) {
        const token = await page.evaluate(() => {
            return localStorage.getItem("id_token")
        })
        return token
    }
}
