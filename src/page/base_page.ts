import { Locator, Page } from "@playwright/test";

export class MenuBar {
    page: Page
    billingTab: Locator
    paymentTab: Locator
    myAdsAccountTab: Locator
    constructor (page: Page) {
        this.page = page
        this.billingTab = this.page.getByTestId('tab-layout.textbilling-navbar')
    }
    async clickBillingTab() {
        await this.billingTab.click()
        await this.page.waitForTimeout(5000)
    }
}