import { Locator, Page, expect } from "@playwright/test";

export class BillingPage {
    page: Page
    pageTitle: Locator
    changePlanButton: Locator
    planName: Locator
    feeMonthlyPLan: Locator
    cancelButton: Locator
    limitAdsAccount: Locator
    limitSpending: Locator
    support: Locator
    trialDay: Locator
    creditCardFee: Locator
    payoneerFee: Locator
    paypalFee: Locator
    USDTFee: Locator
    airwallexFee: Locator
    wiseFee: Locator
    tazapayFee: Locator
    lianLianFee: Locator
    lastNumberCard: Locator
    listBillingHistory: Locator
    constructor(page: Page) {
        this.page = page
        this.pageTitle = this.page.getByText(' Billing detail ')
        this.changePlanButton = this.page.locator('xpath=//*[@id="trigger-change-plan"]')
        this.planName = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div[1]/div/div[1]/div[1]/span')
        this.feeMonthlyPLan = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div[1]/div/div[1]/h1')
        this.cancelButton = this.page.getByText(' Cancel plan ')
        this.limitAdsAccount = this.page.getByText('Maximum manage 1 Ad Account')
        this.limitSpending = this.page.getByText('Unlimited spending')
        this.support = this.page.getByText('Support 1/1')
        this.trialDay = this.page.getByText('1 trial day(s) ')
        this.creditCardFee = this.page.getByText(' Credit Card: 3% ')
        this.payoneerFee = this.page.getByText(' Payoneer: 3% ')
        this.paypalFee = this.page.getByText(' PayPal: 3% ')
        this.USDTFee = this.page.getByText(' USDT: 3% ')
        this.airwallexFee = this.page.getByText(' Airwallex: 3% ')
        this.wiseFee = this.page.getByText(' Wise: 3% ')
        this.tazapayFee = this.page.getByText(' Tazapay: 3% ')
        this.lianLianFee = this.page.getByText(' LianLian: 3% ')
        this.lastNumberCard = this.page.locator('//*[@id="app"]/div/div/div[3]/div[3]/div/div/div/div/div[1]/div/div[1]/div[3]/div/span[2]')
        this.listBillingHistory = this.page
        .locator('//*[@id="__BVID__160"]/div/div[1]/div/div[2]/table')
        //xpath=//div[contains(@id,"card-pricing")]
    }

    async verifyLinkBillingTabIsCorrect(page, linkBillingTab) {
        const url = await page.url();
        await expect(url).toBe(linkBillingTab);
    }
    async verifyTitlePageShow(titleExpected) {
        const titleActually = await this.pageTitle.textContent()
        await expect(titleActually).toContain(titleExpected)
    }
    async verifyInfoPlanShowsIsCorrect(name,feePlan) {
        const namePlan = await this.planName.textContent()
        await expect(namePlan).toContain(name)
        const feeMonthlyPlan = await this.feeMonthlyPLan.textContent()
        await expect(feeMonthlyPlan).toContain(feePlan)
    }
    async verifyBenefitPackageShowIsCorrect(limitAdsAccount, limitSpending, support, trialDay, {creditCardFee, paypalFee, payoneerFee, USDTFee, airwallexFee, wiseFee, tazapayFee, lianlianFee}) {
        const limitAdsAccountActually = await this.limitAdsAccount.textContent()
        const limitSpendingActually = await this.limitSpending.textContent()
        const supportActually = await this.support.textContent()
        const trialDayActually = await this.trialDay.textContent()
        const creditCardFeeActually = await this.creditCardFee.textContent()
        const payoneerFeeActually = await this.payoneerFee.textContent()
        const paypalFeeActually = await this.paypalFee.textContent()
        const USDTFeeActually = await this.USDTFee.textContent()
        const wiseFeeActually = await this.wiseFee.textContent()
        const airwallexFeeActually = await this.airwallexFee.textContent()
        const tazapayFeeActually = await this.tazapayFee.textContent()
        const lianLianFeeActually = await this.lianLianFee.textContent()
        await expect(limitAdsAccountActually).toContain(limitAdsAccount)
        await expect(limitSpendingActually).toContain(limitSpending)
        await expect(supportActually).toContain(support)
        await expect(trialDayActually).toContain(trialDay)
        await expect(creditCardFeeActually).toContain(creditCardFee)
        await expect(payoneerFeeActually).toContain(payoneerFee)
        await expect(paypalFeeActually).toContain(paypalFee)
        await expect(USDTFeeActually).toContain(USDTFee)
        await expect(wiseFeeActually).toContain(wiseFee)
        await expect(airwallexFeeActually).toContain(airwallexFee)
        await expect(lianLianFeeActually).toContain(lianlianFee)
        await expect(tazapayFeeActually).toContain(tazapayFee)
    }
    async verifyChangePlanButtonShow(titleButton) {
        const titleChangePlanButton = await this.changePlanButton.textContent()
        await expect(this.changePlanButton).toBeVisible()
        await expect(titleChangePlanButton).toContain(titleButton)
    }
    async verifyCancelButtonShow(titleButton) {
        const titleCancelButton = await this.cancelButton.textContent()
        await expect(this.cancelButton).toBeVisible()
        await expect(titleCancelButton).toContain(titleButton)
    }
    async clickOnChangePlanButton() {
        await this.changePlanButton.click()
        await this.page.waitForTimeout(5000)
    }
    async getListCreditCard(request, token) {
        const response = await request.get('/api/billings/stripe/payment-methods?lang=en', {
                headers: {
                  Authorization: `Bearer ${token}`,
                }
        })
        const listCard = await response.json()
        return listCard
    }
    async getLastNumberCardDefault(response) {
        let card;
        if (response.status === 200) {
            const cardList = response.result;
            if (cardList.length > 0) {
                card = cardList.find(card => { return card.default === true});
            }
        }
        return card.last4
    }
    async verifyCreditCardShowOnBillingPageIsCorrect(lastNumberCardDefault) {
        const lastNumberCardShow = await this.lastNumberCard.textContent()
        await expect(lastNumberCardDefault).toBe(lastNumberCardShow)
    }
}