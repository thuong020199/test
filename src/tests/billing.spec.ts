import { dataTest } from "../data/billing";
import { BillingPage } from "../page/billing_page";
import { LoginPage } from "../page/login_page";
import { MenuBar } from "../page/base_page";
import { PricingPlanPage } from "../page/pricing_plan_page";
import { request } from "http";

const { test, expect } = require('@playwright/test');
const { timeout } = require('../../playwright.config');
const { chromium } = require('playwright');

let browser;
let page;

test.describe('test billing page', () => {
    test.beforeAll('Login success', async () => {
        browser = await chromium.launch()
        page = await browser.newPage()

        const login = new LoginPage(page)
        await login.goto()
        await login.enterLogin()
        await login.clickOnButtonLogin()
    })
    test.afterAll(async () => {
        browser.close()
    })
    test.beforeEach('Open tab Billing detail success', async () => {
        const menu_bar = new MenuBar(page)
        await menu_bar.clickBillingTab()
    })
    test(dataTest.TC_01.name, async ({ request }) => {
        const loginPage = new LoginPage(page)
        const token = await loginPage.getToken(page)
        const billingPage = new BillingPage(page)
        await billingPage.verifyLinkBillingTabIsCorrect(page, dataTest.TC_01.testData.url)
        await billingPage.verifyTitlePageShow(dataTest.TC_01.testData.title)
        await billingPage.verifyChangePlanButtonShow(dataTest.TC_01.testData.titleChangePlanButton)
        await billingPage.verifyCancelButtonShow(dataTest.TC_01.testData.titleCancelButton)
        await billingPage.verifyInfoPlanShowsIsCorrect(dataTest.TC_01.testData.namePlan, dataTest.TC_01.testData.feeMonthlyPlan)
        await billingPage.verifyBenefitPackageShowIsCorrect(
            dataTest.TC_01.testData.benefitPackage.limitAdsAccount,
            dataTest.TC_01.testData.benefitPackage.limitSpending,
            dataTest.TC_01.testData.benefitPackage.support,
            dataTest.TC_01.testData.benefitPackage.trialDay,
            dataTest.TC_01.testData.benefitPackage.serviceFee
        )
        const response = await billingPage.getListCreditCard(request, token)
        const lastNumberCardDefault = await billingPage.getLastNumberCardDefault(response)
        await billingPage.verifyCreditCardShowOnBillingPageIsCorrect(lastNumberCardDefault)
    })
})

