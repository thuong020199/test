import { BillingPage } from "../page/billing_page";
import { LoginPage } from "../page/login_page";
import { MenuBar } from "../page/base_page";
import { PricingPlanPage } from "../page/pricing_plan_page";
import { dataTest } from "../data/pricing_plan";
import { request } from "http";

const { test, expect } = require('@playwright/test');
const { timeout } = require('../../playwright.config');
const { chromium } = require('playwright');
let browser
let page

test.describe('Test pricing plan', () => {
    test.beforeAll('Login success', async () => {
        browser = await chromium.launch()
        page = await browser.newPage()

        const login = new LoginPage(page)
        await login.goto()
        await login.enterLogin()
        await login.clickOnButtonLogin()
    })
    test.afterAll(async () => {
        browser.close()
    })
    test.beforeEach('Open tab Billing detail success', async () => {
        const menu_bar = new MenuBar(page)
        await menu_bar.clickBillingTab()
    })
    test(dataTest.TC_02.name, async () => {
        const billingPage = new BillingPage(page)
        const pricingPage = new PricingPlanPage(page)
        await billingPage.clickOnChangePlanButton()
        await pricingPage.verifyTitlePricingPageShowIsCorrect(dataTest.TC_02.testData.title)
        await pricingPage.verifyLinkPricingPageIsCorrect(page)
        await pricingPage.verifyTrialDayShowOnText(dataTest.TC_02.testData.textTrialDay)
        await pricingPage.verifyInfoRegularPlanShow(
            dataTest.TC_02.testData.popularPlan.namePlan,
            dataTest.TC_02.testData.popularPlan.feePlan,
            dataTest.TC_02.testData.popularPlan.maximumAdsAccount,
            dataTest.TC_02.testData.popularPlan.limitSpending,
            dataTest.TC_02.testData.popularPlan.trialDay
        )
        await pricingPage.verifyVisibleUpgradeRegularPlanButton()
        await pricingPage.verifyInfoEnterprisePlanShow(
            dataTest.TC_02.testData.enterprisePlan.namePlan,
            dataTest.TC_02.testData.enterprisePlan.feePlan,
            dataTest.TC_02.testData.enterprisePlan.maximumAdsAccount,
            dataTest.TC_02.testData.enterprisePlan.limitSpending
        )
        await pricingPage.verifyVisibleUpgradeEnterprisePlanButton()
    })
    test(dataTest.TC_03.name, async ({ request }) => {
        const login_page = new LoginPage(page)
        const token = await login_page.getToken(page)
        const billingPage = new BillingPage(page)
        const pricingPage = new PricingPlanPage(page)
        await billingPage.clickOnChangePlanButton()
        await pricingPage.clickOnUpgradeRegularButton()
        await pricingPage.verifyListCreditCardShow()
        const totalCardOnUI = await pricingPage.countTotalCard()
        const listCard = await pricingPage.getListCreditCard(request, token)
        const totalCardOnApi = await pricingPage.getTotalCard(listCard)
        await pricingPage.verifyTotalCardShowIsCorrect(totalCardOnUI, totalCardOnApi)
    })
    test(dataTest.TC_04.name, async () => {
        const billingPage = new BillingPage(page)
        const pricingPage = new PricingPlanPage(page)
        await billingPage.clickOnChangePlanButton()
        await pricingPage.clickOnUpgradeRegularButton()
        await pricingPage.clickOnAddCreditCardButton()
        await pricingPage.verifyVisibleCloseButtonAfterOpenAddCardForm()
        await pricingPage.verifyHiddenAddCreditCardButtonAfterOpenAddCardForm()
        await pricingPage.verifyAddCreditCardFormShow()
    })
    test(dataTest.TC_05.name, async () => {
        const billingPage = new BillingPage(page)
        const pricingPage = new PricingPlanPage(page)
        await billingPage.clickOnChangePlanButton()
        await pricingPage.clickOnUpgradeRegularButton()
        await pricingPage.clickOnAddCreditCardButton()
        await pricingPage.clickOnCloseAddCardFormButton()
        await pricingPage.verifyAddCardFormIsClosed()
    })
    test(dataTest.TC_06.name, async () => {
        const billingPage = new BillingPage(page)
        const pricingPage = new PricingPlanPage(page)
        await billingPage.clickOnChangePlanButton()
        await pricingPage.clickOnUpgradeRegularButton()
        await pricingPage.clickOnAddCreditCardButton()
        await pricingPage.clickAddCardButton()
        await pricingPage.verifyEmailMessageErrorShow(dataTest.TC_06.testData.emailMessageError)
        await pricingPage.verifyCardNumberMessageErrorShow(dataTest.TC_06.testData.cardNumberMessageError)
        await pricingPage.verifyExpiredMessageErrorShow(dataTest.TC_06.testData.expiredMessageError)
        await pricingPage.verifyCVCMessageErrorShow(dataTest.TC_06.testData.cvcMessageError)
        await pricingPage.verifyNameCardMessageErrorShow(dataTest.TC_06.testData.nameCardMessageError)
    })
    test(dataTest.TC_07.name, async () => {
        const billingPage = new BillingPage(page)
        const pricingPage = new PricingPlanPage(page)
        await billingPage.clickOnChangePlanButton()
        await pricingPage.clickOnUpgradeRegularButton()
        await pricingPage.clickOnAddCreditCardButton()
        const totalCardBeforeAddCard = await pricingPage.countTotalCard()
        await pricingPage.enterCardData(
            dataTest.TC_07.testData.email,
            dataTest.TC_07.testData.cardNumber,
            dataTest.TC_07.testData.expired,
            dataTest.TC_07.testData.cvc,
            dataTest.TC_07.testData.nameCard
        )
        await pricingPage.clickAddCardButton()
        await page.waitForTimeout(5000)
        await pricingPage.verifyAddCardFormIsClosed()
        const totalCardAfterAddCard = await pricingPage.countTotalCard()
        await pricingPage.verifyTotalCardShowAfterAddCard(totalCardBeforeAddCard, totalCardAfterAddCard)
    })
})