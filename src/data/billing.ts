export const dataTest= {
    TC_01: {
        name: 'Verify Billing detail page shows when clicking on Billing link in menu bar',
        testData: {
            url: 'https://dev-tiktok.ecomdy.com/billing',
            title: ' Billing detail ',
            namePlan: ' regular_1 ',
            feeMonthlyPlan: ' $9 ',
            titleChangePlanButton: ' Change plan ',
            titleCancelButton: ' Cancel plan ',
            benefitPackage: {
                limitAdsAccount: 'Maximum manage 1 Ad Account',
                limitSpending: 'Unlimited spending',
                trialDay: '1 trial day(s) ',
                support: 'Support 1/1',
                serviceFee: {
                    creditCardFee: ' Credit Card: 3% ',
                    paypalFee: ' PayPal: 3% ',
                    payoneerFee: ' Payoneer: 3% ',
                    USDTFee: ' USDT: 3% ',
                    airwallexFee: ' Airwallex: 3% ',
                    wiseFee: ' Wise: 3% ',
                    tazapayFee: ' Tazapay: 3% ',
                    lianlianFee: ' LianLian: 3% '
                }
            }
        }
    }
}