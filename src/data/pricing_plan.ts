export const dataTest = {
    TC_02: {
        name: 'Verify Pricing Plan page shows when clicking on Change plan button',
        testData: {
            title: ' Pricing Plan ',
            textTrialDay: ' Try Ecomdy Media for 1 days to start advertising on Ecomdy Media TikTok Ads. Easy cancel any time. ',
            popularPlan: {
                namePlan: ' Regular ',
                feePlan: ' $9 ',
                maximumAdsAccount: 'Maximum manage 1 ads account',
                limitSpending: 'Unlimited spending',
                trialDay: '1 days trial'
            },
            enterprisePlan: {
                namePlan: ' Enterprise ',
                feePlan: ' $249 ',
                maximumAdsAccount: 'Maximum manage 50 ads account',
                limitSpending: 'Unlimited spending'
            }
        }
    },
    TC_03: {
        name: 'Verify Credit card list shows when clicking on Upgrade button',
        testData: {}
    },
    TC_04: {
        name: 'Verify Add credit card from shows when clicking on + Add credit card button',
        testData: {}
    },
    TC_05: {
        name: 'Verify Add credit card form is closed when clicking on Close button',
        testData: {}
    },
    TC_06: {
        name: 'Verify user cannot add credit card if all fields are empty',
        testData: {
            emailMessageError: ' Your email is incomplete. ',
            cardNumberMessageError: " Your card's number is incomplete. ",
            expiredMessageError: " Your card's expiration date is incomplete. ",
            cvcMessageError: " Your card's security code is incomplete. ",
            nameCardMessageError: " Your cardholder's name is incomplete. "
        }
    },
    TC_07: {
        name: 'Verify user can add credit card successfully when all fields are filled out with valid data',
        testData: {
            email: 'thuong@gmail.com',
            cardNumber: '4242 4242 4242 4242',
            expired: '12/25',
            cvc: '123',
            nameCard: 'thuong'
        }
    }
}